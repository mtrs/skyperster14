using Content.Shared.Examine;
using Content.Shared.IdentityManagement;
using Content.Shared.Verbs;
using Content.Shared.Sirena;
using Robust.Shared.Utility;
using Content.Server.EUI;
using Content.Server.Skyperster.DetailExaminable;
using Content.Shared.Skyperster.DetailExaminable;
using Robust.Shared.Player;
using Content.Server.Actions;
using Content.Server.Administration;

namespace Content.Server.DetailExaminable
{
    public sealed class DetailExaminableSystem : EntitySystem
    {
        [Dependency] private readonly ExamineSystemShared _examineSystem = default!;
        [Dependency] private readonly EuiManager _euis = default!; // skyperster
        [Dependency] private readonly ActionsSystem _actions = default!; // skyperster
        [Dependency] private readonly QuickDialogSystem _quickDialog = default!; // skyperster

        public override void Initialize()
        {
            base.Initialize();

            SubscribeLocalEvent<DetailExaminableComponent, GetVerbsEvent<ExamineVerb>>(OnGetExamineVerbs);
            SubscribeLocalEvent<DetailExaminableComponent, MapInitEvent>(OnMapInit); // skyperster
            SubscribeLocalEvent<DetailExaminableComponent, ComponentShutdown>(OnShutdown); // skyperster
            SubscribeLocalEvent<DetailExaminableComponent, ChangePoseActionEvent>(OnChangePoseAction); // skyperster
        }

        // skyperster start
        private void OnMapInit(EntityUid uid, DetailExaminableComponent component, MapInitEvent args)
        {
            // try to add posing action when posing comp added
            _actions.AddAction(uid, ref component.PoseActionEntity, component.ChangePoseAction);
            //component.PoseContent = Loc.GetString("posing-content-none");
        }

        private void OnShutdown(EntityUid uid, DetailExaminableComponent component, ComponentShutdown args)
        {
            // remove scream action when component removed
            if (component.PoseActionEntity != null)
            {
                _actions.RemoveAction(uid, component.PoseActionEntity);
            }
        }
        // skyperster end

        private void OnGetExamineVerbs(EntityUid uid, DetailExaminableComponent component, GetVerbsEvent<ExamineVerb> args)
        {
            if (Identity.Name(args.Target, EntityManager) != MetaData(args.Target).EntityName)
                return;

            var detailsRange = _examineSystem.IsInDetailsRange(args.User, uid);
            // skyperster start
            if (!EntityManager.TryGetComponent(args.User, out ActorComponent? actor))
                return;
            var player = actor.PlayerSession;
            // skyperster end
            var verb = new ExamineVerb()
            {
                Act = () =>
                {
                    _euis.OpenEui(new FlavorPanelEui(GetNetEntity(args.Target)), player); // skyperster
                },
                Text = Loc.GetString("detail-examinable-verb-text"),
                Category = VerbCategory.Examine,
                Disabled = !detailsRange,
                Message = detailsRange ? null : Loc.GetString("detail-examinable-verb-disabled"),
                Icon = new SpriteSpecifier.Texture(new ("/Textures/Interface/VerbIcons/examine.svg.192dpi.png"))
            };

            args.Verbs.Add(verb);
        }

        // skyperster start
        private void OnChangePoseAction(EntityUid uid, DetailExaminableComponent component, ChangePoseActionEvent args)
        {
            if (args.Handled)
                return;

            if (!EntityManager.TryGetComponent(args.Performer, out ActorComponent? actor))
                return;

            var player = actor.PlayerSession;

            _quickDialog.OpenDialog(player,
                                Loc.GetString("posing-dialog-name"),
                                Loc.GetString("posing-dialog-field-pose"),
                                (LongString newPose) =>
            {
                if (newPose.String == string.Empty)
                {
                    component.PoseContent = Loc.GetString("posing-content-none");
                }
                else
                {
                    component.PoseContent = newPose.String;
                }
            });
            args.Handled = true;
        }
        // skyperster end
    }
}
