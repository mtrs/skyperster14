using Robust.Shared.Configuration;

namespace Content.Shared.Skyperster.CCVar;

[CVarDefs]
public sealed class SkypersterCVars
{
    /// <summary>
    /// Whether or not respawning is enabled.
    /// </summary>
    public static readonly CVarDef<bool> RespawnEnabled =
        CVarDef.Create("skp.respawn.enabled", true, CVar.SERVER | CVar.REPLICATED);

    /// <summary>
    /// Respawn time, how long the player has to wait in seconds after death.
    /// </summary>
    public static readonly CVarDef<float> RespawnTime =
        CVarDef.Create("skp.respawn.time", 300.0f, CVar.SERVER | CVar.REPLICATED);
}
