ent-Cartridge223 = патрон (.223 винтовочный)
    .desc = { ent-BaseCartridgeLightRifle.desc }

ent-Cartridge223Rubber = патрон (.223 винтовочный резиновый)
    .desc = { ent-BaseCartridgeLightRifle.desc }

ent-Cartridge223AP = патрон (.223 винтовочный бронебойный)
    .desc = { ent-BaseCartridgeLightRifle.desc }
