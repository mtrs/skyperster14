﻿using Content.Shared.Sirena;
using Robust.Shared.Serialization.TypeSerializers.Implementations.Custom.Prototype;
using Robust.Shared.Prototypes;

namespace Content.Server.DetailExaminable

{
    [RegisterComponent]
    public sealed partial class DetailExaminableComponent : Component
    {
        [DataField("content", required: true)] [ViewVariables(VVAccess.ReadWrite)]
        public string Content = "";

        [DataField("poseContent")]
        [ViewVariables(VVAccess.ReadWrite)]
        public string PoseContent = "";

        [DataField("changePoseAction", customTypeSerializer: typeof(PrototypeIdSerializer<EntityPrototype>))]
        [AutoNetworkedField]
        public string ChangePoseAction = "ActionChangePose";

        [DataField("poseActionEntity")]
        [AutoNetworkedField]
        public EntityUid? PoseActionEntity;

    }
}
