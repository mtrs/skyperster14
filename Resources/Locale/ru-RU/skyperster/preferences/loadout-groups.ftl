loadout-group-passenger-id = КПК пассажира
loadout-group-passenger-head = Голова пассажира
loadout-group-senior-engineer-id = КПК ведущего инженера
loadout-group-senior-engineer-jumpsuit = Комбинезон ведущего инженера
loadout-group-senior-engineer-head = Голова ведущего инженера
loadout-group-senior-physician-id = КПК дежурного врача
loadout-group-senior-physician-jumpsuit = Комбинезон дежурного врача
loadout-group-senior-physician-head = Голова дежурного врача
loadout-group-viro-jumpsuit = Комбинезон вирусолога
loadout-group-viro-head = Голова вирусолога
loadout-group-viro-id = КПК вирусолога
loadout-group-viro-outer-clothing = Верхняя одежда вирусолога
loadout-group-senior-viro-back = Рюкзак вирусолога
loadout-group-roboticist-head = Голова робототехника
loadout-group-roboticist-jumpsuit = Комбинезон робототехника
loadout-group-roboticist-outerclothing = Верхняя одежда робототехника
loadout-group-roboticist-shoes = Обувь робототехника
loadout-group-roboticist-gloves = Перчатки робототехника
loadout-group-roboticist-id = КПК робототехника
loadout-group-seniorres-id = КПК ведущего учёного
loadout-group-seniorres-jumpsuit = Комбинезон ведущего учёного
loadout-group-seniorres-outer = Верхняя одежда ведущего учёного
loadout-group-senoff-pda = КПК инструктора СБ
loadout-group-senoff-suit = Комбинезон инструктора СБ
loadout-group-brigmed-suit = Комбинезон бригмедика
loadout-group-brigmed-head = Голова бригмедика
loadout-group-brigmed-outer = Верхняя одежда бригмедика
loadout-group-brigmed-backpack = Рюкзак бригмедика
loadout-group-brigmed-id = КПК бригмедика
loadout-group-underwear-bottom = Нижнее бельё (низ)
loadout-group-underwear-top = Нижнее бельё (верх)
loadout-group-underwear-socks = Носки
loadout-group-generic-id = КПК
