guide-entry-E2 = ERP не суть проекта!
guide-entry-E3 = Nuh-Uh контент
guide-entry-E4 = Стоять! ЕРП-полиция!
guide-entry-E5 = Эксгибиционизм
guide-entry-skyperster = skyperster14
guide-entry-rphelpskp = Помощь с отыгрышем
guide-entry-timeaddskp = Добавление игрового времени
guide-entry-infoskp = Дополнительная информация
guide-entry-height-cd = Рост персонажа
guide-entry-stationprefix = Префиксы станций

# ЕРП
guide-entry-ERPMain = Про ЕРП
# ЕРП статусы
guide-entry-ERPStatus = Про статусы
guide-entry-ERPStatusNo = Отказ от ERP
guide-entry-ERPStatusAsk = Неполное ERP
guide-entry-ERPStatusYes = Полное ERP

# РП
guide-entry-ultimaterpguide = Ультимативный РП-гайд
guide-entry-flavorguide = Флаворы и позинг
guide-entry-emotesguide = Рекомендации по эмоутам
guide-entry-erpemotesguide = ЕРП-рекомендации
guide-entry-charactertransfer = Перенос персонажа с другого сервера
guide-entry-roll-command-skp = Волшебство команды roll

# правила
guide-entry-skypersterrules = ЧИТАЙ ПРАВИЛА АЛЁ

guide-entry-skyperster-pizda = КЗ
