using Content.Server.DetailExaminable;
using Content.Shared.Chemistry.Reagent;
using Content.Shared.Sirena;
using Robust.Shared.Prototypes;
using Content.Shared.Skyperster.Humanoid;
using Content.Shared.Humanoid;
using Content.Server.EntityEffects.EffectConditions;
using Content.Server.EntityEffects.Effects;
using Content.Shared.EntityEffects;

namespace Content.Server.Sirena.Chemistry.ReagentEffectsCondition;

public sealed partial class ERPStatusCondition : EntityEffectCondition
{
    [DataField("erp")]
    public ERPStatus ERP = default!;

    [DataField]
    public bool ShouldHave = true;

    public override bool Condition(EntityEffectBaseArgs args)
    {
        if (!args.EntityManager.HasComponent<HumanoidAppearanceComponent>(args.TargetEntity))
        {
            return ERP == 0 ? ShouldHave : false;
        }

        return args.EntityManager.GetComponent<HumanoidAppearanceComponent>(args.TargetEntity).ERPStatus == ERP ? ShouldHave : !ShouldHave;
    }

    public override string GuidebookExplanation(IPrototypeManager prototype)
    {
        return Loc.GetString("reagent-effect-condition-guidebook-erpstatus", ("erp", ERP), ("shouldHave", ShouldHave));
    }
}
