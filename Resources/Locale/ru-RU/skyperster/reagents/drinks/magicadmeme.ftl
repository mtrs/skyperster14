# ▬▬ Пол: М ▬▬
# PopupMessage
magicadmeme-popupmessage-s1-m-1 = Вы ощущаете странное чувство во рту
magicadmeme-popupmessage-s1-m-2 = В вашем животе появляется неприятное чувство
magicadmeme-popupmessage-s1-m-3 = Вы ощущаете ужасный привкус на языке

magicadmeme-popupmessage-s2-m-1 = Вы чувствуете себя усталым
magicadmeme-popupmessage-s2-m-2 = Вы ощущаете небольшое раздражение
magicadmeme-popupmessage-s2-m-3 = Ваш живот немного болит

magicadmeme-popupmessage-s3-m-1 = Вы чувствуете, словно вас сейчас вырвет
magicadmeme-popupmessage-s3-m-2 = Вы ощущаете сильную боль в животе
magicadmeme-popupmessage-s3-m-3 = Вы чувствуете, что по вашему телу пробегается холод

magicadmeme-popupmessage-s4-m-1 = Вы ощущаете, словно ваши внутренности горят
magicadmeme-popupmessage-s4-m-2 = Ваше тело замедляется, а боль усиливается
magicadmeme-popupmessage-s4-m-3 = Ваш живот ужасно болит

# AddPushMarkup
magicadmeme-pushmarkup-s1-m = Парень выглядит слегка захворавшим.
magicadmeme-pushmarkup-s2-m = Парень сильно потеет. Он томно дышит.
magicadmeme-pushmarkup-s3-m = Парня слегка трясет. Он выглядит бледновато.
magicadmeme-pushmarkup-s4-m = Парня сильно трясёт. Он дышит очень неровно. Он выглядит бледным.

# ▬▬ Пол: Ж ▬▬
# PopupMessage
magicadmeme-popupmessage-s1-fm-1 = Вы ощущаете странное чувство во рту
magicadmeme-popupmessage-s1-fm-2 = В вашем животе появляется неприятное чувство
magicadmeme-popupmessage-s1-fm-3 = Вы ощущаете ужасный привкус на языке

magicadmeme-popupmessage-s2-fm-1 = Вы чувствуете себя усталой
magicadmeme-popupmessage-s2-fm-2 = Вы ощущаете небольшое раздражение
magicadmeme-popupmessage-s2-fm-3 = Ваш живот немного болит

magicadmeme-popupmessage-s3-fm-1 = Вы чувствуете, словно вас сейчас вырвет
magicadmeme-popupmessage-s3-fm-2 = Вы ощущаете сильную боль в животе
magicadmeme-popupmessage-s3-fm-3 = Вы чувствуете, что по вашему телу пробегается холод

magicadmeme-popupmessage-s4-fm-1 = Вы ощущаете, словно ваши внутренности горят
magicadmeme-popupmessage-s4-fm-2 = Ваше тело замедляется, а боль усиливается
magicadmeme-popupmessage-s4-fm-3 = Ваш живот ужасно болит

# AddPushMarkup
magicadmeme-pushmarkup-s1-fm = Девушка выглядит слегка захворавшей.
magicadmeme-pushmarkup-s2-fm = Девушка сильно потеет. Она томно дышит.
magicadmeme-pushmarkup-s3-fm = Девушку слегка трясет. Она выглядит бледновато.
magicadmeme-pushmarkup-s4-fm = Девушку сильно трясёт. Она дышит очень неровно. Она выглядит бледной.

# ▬▬ Пол: Unsex ▬▬
# PopupMessage
magicadmeme-popupmessage-s1-u-1 = TODO
magicadmeme-popupmessage-s1-u-2 = TODO
magicadmeme-popupmessage-s1-u-3 = TODO

magicadmeme-popupmessage-s2-u-1 = TODO
magicadmeme-popupmessage-s2-u-2 = TODO
magicadmeme-popupmessage-s2-u-3 = TODO

magicadmeme-popupmessage-s3-u-1 = TODO
magicadmeme-popupmessage-s3-u-2 = TODO
magicadmeme-popupmessage-s3-u-3 = TODO

magicadmeme-popupmessage-s4-u-1 = TODO
magicadmeme-popupmessage-s4-u-2 = TODO
magicadmeme-popupmessage-s4-u-3 = TODO

# AddPushMarkup
magicadmeme-pushmarkup-s1-u = TODO
magicadmeme-pushmarkup-s2-u = TODO
magicadmeme-pushmarkup-s3-u = TODO
magicadmeme-pushmarkup-s4-u = TODO
