using Content.Server.Cargo.Components;
using Content.Server.Cargo.Systems;
using Robust.Server.GameObjects;
using Robust.Server.Maps;
using Robust.Shared.Map;
using Content.Server.GameTicking.Rules.Components;
using Content.Server.Salvage;
using Content.Server.Shuttles.Components;
using Content.Server.Shuttles.Systems;
using Content.Server.StationEvents.Components;
using Robust.Shared.Player;
using Robust.Shared.Random;
using Content.Shared.GameTicking.Components;
using Content.Server.Station.Components;

namespace Content.Server.StationEvents.Events;

public sealed class BluespaceErrorRule : StationEventSystem<BluespaceErrorRuleComponent>
{
    [Dependency] private readonly MapLoaderSystem _mapLoader = default!;
    [Dependency] private readonly ShuttleSystem _shuttle = default!;
    [Dependency] private readonly IRobustRandom _random = default!;
    [Dependency] private readonly SharedTransformSystem _transform = default!;
    [Dependency] private readonly PricingSystem _pricing = default!;
    [Dependency] private readonly CargoSystem _cargo = default!;
    [Dependency] private readonly SharedMapSystem _map = default!;

    protected override void Started(EntityUid uid, BluespaceErrorRuleComponent component, GameRuleComponent gameRule,
        GameRuleStartedEvent args)
    {
        base.Started(uid, component, gameRule, args);

        var shuttleMap = _map.CreateMap(out var mapId, true);
        Log.Info($"Created map {mapId} for {ToPrettyString(uid):rule}");
        var options = new MapLoadOptions
        {
            LoadMap = true,
        };

        if (!_mapLoader.TryLoad(mapId, component.GridPath, out var gridUids, options))
        {
            Log.Error($"Failed to load map from {component.GridPath}!");
            Del(shuttleMap);
            ForceEndSelf(uid, gameRule);
            return;
        }
        component.GridUid = gridUids[0];
        if (component.GridUid is not EntityUid gridUid)
            return;
        component.StartingValue = _pricing.AppraiseGrid(gridUid);
        _shuttle.SetIFFColor(gridUid, component.Color);
        var offset = _random.NextVector2(600f, 1450f);
        Random angleOffsetRnd = new Random();
        int angleOffset = angleOffsetRnd.Next(0, 360);
        var stationMapId = GameTicker.DefaultMap;
        var coords = new MapCoordinates(offset, stationMapId);
        var location = Spawn(null, coords);
        var entCoords = new EntityCoordinates(location, offset);
        if (TryComp<ShuttleComponent>(component.GridUid, out var shuttle))
        {
            _shuttle.FTLToCoordinates(gridUid, shuttle, entCoords, angleOffset, 5.5f, 15f);
        }

    }

    protected override void Ended(EntityUid uid, BluespaceErrorRuleComponent component, GameRuleComponent gameRule, GameRuleEndedEvent args)
    {
        base.Ended(uid, component, gameRule, args);

        if (!EntityManager.TryGetComponent<TransformComponent>(component.GridUid, out var gridTransform))
        {
            Log.Error("bluespace error objective was missing transform component");
            return;
        }

        if (gridTransform.GridUid is not EntityUid gridUid)
        {
            Log.Error( "bluespace error has no associated grid?");
            return;
        }

        var gridValue = _pricing.AppraiseGrid(gridUid, null);
        foreach (var player in Filter.Empty().AddInGrid(gridTransform.GridUid.Value, EntityManager).Recipients)
        {
            if (player.AttachedEntity.HasValue)
            {
                var playerEntityUid = player.AttachedEntity.Value;
                _transform.SetParent(playerEntityUid, gridTransform.ParentUid);
            }
        }
        // Deletion has to happen before grid traversal re-parents players.
        Del(gridUid);

    }
}
