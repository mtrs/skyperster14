using Content.Server.Sirnea.EntitySystems;
using Content.Shared.Chemistry.Reagent;
using Content.Shared.Damage;
using Robust.Shared.Prototypes;
using Content.Server.EntityEffects.EffectConditions;
using Content.Server.EntityEffects.Effects;
using Content.Shared.EntityEffects;

namespace Content.Server.Sirena.Chemistry.ReagentEffects;

public sealed partial class Revival : EntityEffect
{
    [DataField]
    public DamageSpecifier ChangeDamage = default!;

    [DataField]
    public bool RevivalRotten = true;
    public override void Effect(EntityEffectBaseArgs args)
    {
        var system = args.EntityManager.EntitySysManager.GetEntitySystem<RevivalSystem>();
        system.Revival(args.TargetEntity, ChangeDamage, RevivalRotten);
    }
    protected override string? ReagentEffectGuidebookText(IPrototypeManager prototype, IEntitySystemManager entSys)
        => Loc.GetString("reagent-effect-guidebook-revival-effect");
}
