ent-PillCanisterAmbuzolPlus = { ent-PillCanister }
    .suffix = Амбузол плюс, 10
    .desc = { ent-PillCanister.desc }

ent-PillCanisterGabapentin = { ent-PillCanister }
    .suffix = габопептан, 15
    .desc = Нейролептик представляет собой класс фармацевтических препаратов, который характеризуется способностью снижать нейрочувствительность и может применяться в лечении судорог.

ent-PillCanisterEmoAplife = { ent-PillCanister }
    .suffix = эмоаплайф, 15
    .desc = Это инновационный нейростимулятор, предназначенный для глубокого улучшения эмоционального опыта.

ent-PillGabapentin = таблетка габопептана (15u)
    .desc = { ent-Pill.desc }

ent-PillEmoAplife = таблетка эмоаплайф (15u)
    .desc = { ent-Pill.desc }
