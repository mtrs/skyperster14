ent-JudgePDA = КПК судьи
    .desc = Пахнет бумагой.
ent-CookPDA = КПК повара
    .desc = Покрыт жиром и мукой.
ent-ViroPDA = КПК вирусолога
    .desc = Белый и стерильный. Пахнет спиртом. Имеет встроенный анализатор здоровья.
ent-RoboPDA = КПК робототехника
    .desc = Пахнет сварочным топливом.
