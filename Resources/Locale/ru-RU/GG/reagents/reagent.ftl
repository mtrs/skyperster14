reagent-name-applecider = яблочный сидр
reagent-desc-applecider = Слабо-алкогольный напиток на основе забродивших яблок.

reagent-name-kalua = калуа
reagent-desc-kalua = Кофейный ликёр под другим взгядом

reagent-name-kuantro = куантро
reagent-desc-kuantro = Апельсиновый ликёр...больше сказать нечего.

reagent-name-lebovsky = домашний лебовски
reagent-desc-lebovsky = ГДЕ ДЕНЬГИ ЛЕБОВСКИ!

reagent-name-bluehawaii = голубая лагуна
reagent-desc-bluehawaii = Цвет напоминающий море.

reagent-name-kamikadze = камикадзе
reagent-desc-kamikadze = БАНЗАЙ!

reagent-name-medovuha = медовуха
reagent-desc-medovuha = Вам действительно нужно описание медовухи?

reagent-name-homeland = дом родной
reagent-desc-homeland = Раньше у него было другое название

reagent-name-brainbomb = взрыв мозга
reagent-desc-brainbomb = Токсичен практически для всего живого, особенно для вашей печени.

reagent-name-clownblood = кровь клоуна
reagent-desc-clownblood = Любимый напиток сотрудников службы безопасности после долгого рабочего дня.

reagent-name-circusjuice = цирковой сок
reagent-desc-circusjuice = Хонкоматерь гордилась бы тобой.

reagent-name-sapopicante = сапо пиканте
reagent-desc-sapopicante = На вкус совсем не похоже на жабу.

reagent-name-graveyard = надгробие
reagent-desc-graveyard = Для тех смен, которые, кажется, никогда не кончатся.

reagent-name-atomicpunch = атомный удар
reagent-desc-atomicpunch = Это НЕ сделает вас неуязвимым для пуль; изотопы тоже включены!

reagent-name-pinkdrink = пинк-дринк
reagent-desc-pinkdrink = Целые цивилизации рушились, пытаясь решить, действительно ли этот напиток имеет розовый вкус...

reagent-name-bubbletea = бабл тии
reagent-desc-bubbletea = Большая соломинка в комплект не входит.

reagent-name-silverjack = сильвер джек
reagent-desc-silverjack = Желаю вам приятных сноведений

reagent-name-orange-creamcicle = сливочное мороженное
reagent-desc-orange-creamcicle = Мороженкооооо!

reagent-physical-desc-glittery = блестящий
