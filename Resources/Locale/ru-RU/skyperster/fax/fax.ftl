fax-machine-message-received = Получена передача на факс "{ $fax }" от "{ $from }".
fax-machine-dialog-field-channel = Радиоканал
fax-machine-popup-channel-invalid = Введён неверный радиоканал.
fax-machine-popup-channel-set = Установлен новый радиоканал для оповещений.
fax-component-sender-name-centcom = ЦентКом
fax-machine-popup-name-needed = Имя факса не должно быть пустым.
