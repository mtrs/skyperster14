humanoid-ingame-erp-status-ask = Игрок попросил [color=yellow]спрашивать[/color] его насчёт ERP-взаимодействия.
humanoid-ingame-erp-status-yes = Игрок [color=green]согласился[/color] на любое ERP-взаимодействие.
humanoid-ingame-erp-status-no = Игрок [color=red]отказался[/color] от какого-либо ERP-взаимодействия.

humanoid-editor-erp-status-no = Отказ от ERP
humanoid-editor-erp-status-ask = Неполное ERP
humanoid-editor-erp-status-yes = Полное ERP

humanoid-profile-editor-erpstatus-label = Статус ERP:
