# Command
bsa-hit-description = Вызывает удар БСА по текущим координатам
bsa-hit-help = Вызывает удар БСА по текущим координатам

# Errors
bsa-hit-coords-error = Системы наведения БСА не смогли навестись на ваши координаты

# BSA Announcement
bsa-hit-announcement = Тревога! Обнаружен огонь блюспейс артелерии! Приготовиться к удару!
bsa-hit-announcer = Автоматическая система оповещения станции
