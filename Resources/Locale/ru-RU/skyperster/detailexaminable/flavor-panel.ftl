flavor-panel-title = Детальный осмотр
flavor-panel-flavortext = Описание:
flavor-panel-pose = Поза:
flavor-panel-erp-yes = ERP-статус: [color=green]Да[/color]
flavor-panel-erp-ask = ERP-статус: [color=yellow]Хз[/color]
flavor-panel-erp-no = ERP-статус: [color=red]Нет[/color]
flavor-panel-flavor-none = [italic]Тут ничего нет.[/italic]
posing-content-none = [italic]Пользователь не установил позу для персонажа. [bolditalic]Sosite[/bolditalic].[/italic]
posing-dialog-name = Позер4000
posing-dialog-field-pose = Новая поза:
posing-verb-text = Поза персонажа
