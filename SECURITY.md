# Сообщения о уязвимостях безопасности
Вы можете сообщить нам об уязвимости в Discord.

Вы можете зайти на [наш Discord-сервер](https://discord.gg/ehEz8BSFw6)
и затем написать в  **личные** сообщения кому-то с ролью `@Хост` или `@Старший Администратор`.

В любом случае, **не разглашайте уязвимость публично, если мы не сказали вам этого сделать**.
