
using Content.Shared.Actions;
using Content.Shared.Actions.Events;
using Robust.Shared.GameStates;
using Robust.Shared.Serialization;

namespace Content.Shared.Sirena.Animations;

/// <summary>
/// </summary>
public sealed partial class EmoteFlipActionEvent : InstantActionEvent { };

/// <summary>
/// </summary>
public sealed partial class EmoteJumpActionEvent : InstantActionEvent { };

/// <summary>
/// </summary>
public sealed partial class EmoteTurnActionEvent : InstantActionEvent { };

[RegisterComponent]
[NetworkedComponent]
public sealed partial class EmoteAnimationComponent : Component
{
    [ViewVariables(VVAccess.ReadWrite)]
    public string AnimationId = "none";

    /*public InstantAction? FlipAction;
    public InstantAction? JumpAction;
    public InstantAction? TurnAction;*/

    [Serializable, NetSerializable]
    public sealed class EmoteAnimationComponentState : ComponentState
    {
        public string AnimationId { get; init; }

        public EmoteAnimationComponentState(string animationId)
        {
            AnimationId = animationId;
        }
    }
}
