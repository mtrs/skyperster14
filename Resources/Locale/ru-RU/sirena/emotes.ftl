emote-flip-action-name = Сальто
emote-flip-action-description = Сделать крутой акработический трюк!
chat-emote-name-flip = Сделать сальто
chat-emote-msg-flip = делает сальто.

emote-jump-action-name = Подпрыгнуть
emote-jump-action-description = Подпрыгни из-за радости или испуга от мыши.
chat-emote-name-jump = Подпрыгнуть
chat-emote-msg-jump = прыгает.

emote-turn-action-name = Кружиться
emote-turn-action-description = You spin me right round baby...
chat-emote-name-turn = Кружиться
chat-emote-msg-turn = кружится.

emote-menu-category-animations = Движения
