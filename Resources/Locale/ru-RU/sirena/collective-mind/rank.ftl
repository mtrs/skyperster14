collective-mind-borg-rank = Юнит
collective-mind-pai-rank = Персональный ИИ

collective-mind-dragon-rank = Дракон
collective-mind-shark-rank = Карпоакула
collective-mind-carp-rank = Карп

collective-mind-queen-rank = Королева
collective-mind-praetorian-rank = Приторианец
collective-mind-ravager-rank = Разрушитель
collective-mind-runner-rank = Бегун
collective-mind-spitter-rank = Плевальщик
collective-mind-drone-rank = Дрон
collective-mind-burrower-rank = Без специализации

collective-mind-rouny-rank = Рунни
