# Command
send-shuttle-description = Спавнит и проигрывает(или нет) сообщение о спавне шаттла
send-shuttle-help = sendshuttle <тип шаттла> (объявляют себя? По умолчанию true)

# Errors
send-shuttle-truefalse-error = Вторая переменная не соответствует ни true, ни false
send-shuttle-shuttletype-error = Обнаружен неизвестный тип шаттла

# Map name
sent-shuttle-map-name = Сектор патруля

shuttle-send-default-announcer = Центральное командование

# ERT
# type: default
shuttle-send-ertdefault-announcement = Внимание! Мы получили запрос на отряд быстрого реагирования. Запрос одобрен. Отряд будет подготовлен и отправлен в кротчайшие сроки.
# type: security
shuttle-send-ertsecurity-announcement = Внимание! Мы получили запрос на отряд быстрого реагирования. Запрос одобрен. Отряд будет подготовлен и отправлен в кротчайшие сроки.
# type: engineer
shuttle-send-ertengineer-announcement = Внимание! Мы получили запрос на отряд быстрого реагирования. Запрос одобрен. Отряд будет подготовлен и отправлен в кротчайшие сроки.
# type: medical
shuttle-send-ertmedical-announcement = Внимание! Мы получили запрос на отряд быстрого реагирования. Запрос одобрен. Отряд будет подготовлен и отправлен в кротчайшие сроки.
# type: janitor
shuttle-send-ertjanitor-announcement = Внимание! Мы получили запрос на отряд быстрого реагирования. Запрос одобрен. Отряд будет подготовлен и отправлен в кротчайшие сроки.

# Others
# type: cbrn
shuttle-send-cbrn-announcement = Внимание! Мы получили запрос на отряд быстрого реагирования. Запрос одобрен. Отряд будет подготовлен и отправлен в кротчайшие сроки.
# type: deathsquad
shuttle-send-deathsquad-announcement = Последняя команда Центральное командование. Просьба экипаж станции оставаться на своих местах. Ожидайте шаттл эвакуации.
# type: denial
shuttle-send-ertdenial-announcement = Внимание! Мы получили запрос на отряд быстрого реагирования. Запрос отклонён. Попытайтесь решить проблемы своими силами.

# Hints
send-shuttle-hint-type = Тип шатта

send-shuttle-hint-type-default = Стандартный отряд ОБР
send-shuttle-hint-type-security = Отряд СБ ОБР
send-shuttle-hint-type-engineer = Отряд инженеров ОБР
send-shuttle-hint-type-medical = Отряд медиков ОБР
send-shuttle-hint-type-janitor = Отряд уборщиков ОБР
send-shuttle-hint-type-cbrn = Отряд РХБЗ
send-shuttle-hint-type-deathsquad = Отряд Эскадрона смерти
send-shuttle-hint-type-denial = Отказ в вызове ОБР

send-shuttle-hint-isannounce = Проигрывать объявление?
send-shuttle-hint-isannounce-true = Да
send-shuttle-hint-isannounce-false = Нет
