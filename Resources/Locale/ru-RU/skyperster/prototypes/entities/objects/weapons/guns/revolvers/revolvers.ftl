ent-WeaponRevolverRex_skp = Рекс
    .desc = Популярный коммерческий револвьер. Использует патроны калибра .45 магнум. 

#RSH-12 

ent-WeaponRevolverRSH12_skp = РШ-12ДС
    .desc = Редкий крупнокалиберный револьвер, изготавливаемый на заказ. Использует патроны калибра 12.7x55мм. 

ent-Cartridge127 = патрон (12.7x55мм)
ent-Cartridge127Rubber = патрон (12.7x55мм резиновый)
ent-Cartridge127AP = патрон (12.7x55мм бронебойный)

ent-SpeedLoader127 = спидлоадер (12.7x55мм)
ent-SpeedLoader127Rubber = спидлоадер (12.7x55мм резиновые)
ent-SpeedLoader127AP = спидлоадер (12.7x55мм бронебойные)