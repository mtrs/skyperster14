using Content.Server.Sirnea.Components;
using Content.Shared.Chemistry.Reagent;
using Robust.Shared.Prototypes;
using Content.Server.EntityEffects.EffectConditions;
using Content.Server.EntityEffects.Effects;
using Content.Shared.EntityEffects;

namespace Content.Server.Sirena.Chemistry.ReagentEffects;

public sealed partial class RmPushMarkup : EntityEffect
{
    public override void Effect(EntityEffectBaseArgs args)
    {
        args.EntityManager.RemoveComponent<PushMarkupEffectComponent>(args.TargetEntity);
    }
    protected override string? ReagentEffectGuidebookText(IPrototypeManager prototype, IEntitySystemManager entSys)
        => Loc.GetString("reagent-effect-guidebook-create-rm-pushmarkup-effect");
}
