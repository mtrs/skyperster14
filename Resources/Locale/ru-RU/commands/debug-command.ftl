cmd-showmarkers-desc = Переключает видимость маркеров, таких как спавны.
cmd-showmarkers-help = Использование: { $command }
cmd-showsubfloor-desc = Делает сущности под полом всегда видимыми.
cmd-showsubfloor-help = Использование: { $command }
cmd-showsubfloorforever-desc = Делает сущности под полом всегда видимыми пока клиент не будет перезапущен.
cmd-showsubfloorforever-help = Использование: { $command }
cmd-notify-desc = Послать уведомление клиента.
cmd-notify-help = Использование: { $command } <message>
