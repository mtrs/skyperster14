ghost-role-information-cburnunitleadersirena-name = лидер отряда РХБЗ
ghost-role-information-cburnunitleadersirena-description = Лидер отряда РХБЗ. Повинуется приказам с ЦК.

ghost-role-information-cburnunitsirena-name = член отряда РХБЗ
ghost-role-information-cburnunitsirena-description = Выполняйте приказы вашего лидера!

ghost-role-information-deadsquadsirena-name = член Эскадрона Смерти
ghost-role-information-deadsquadsirena-description = Ождайте приказа от ОСО...

ghost-role-information-ertleadersirena-name = лидер ОБР
ghost-role-information-ertleadersirena-description = Лидер отряда быстрого реагирования. Повинуется приказам с ЦК.

ghost-role-information-ertengineersirena-name = инженер ОБР
ghost-role-information-ertengineersirena-description = Инженер отряда быстрого реагирования. Ваша первостепенная задача - выполнение приказов вашего лидера!

ghost-role-information-ertjanitorsirena-name = уборщик ОБР
ghost-role-information-ertjanitorsirena-description = Уборщик отряда быстрого реагирования. Ваша первостепенная задача - выполнение приказов вашего лидера!

ghost-role-information-ertmedicalsirena-name = медик ОБР
ghost-role-information-ertmedicalsirena-description = Медик отряда быстрого реагирования. Ваша первостепенная задача - выполнение приказов вашего лидера!

ghost-role-information-ertsecuritysirena-name = офицер безопасности ОБР
ghost-role-information-ertsecuritysirena-description = Офицер безопасности отряда быстрого реагирования. Ваша первостепенная задача - выполнение приказов вашего лидера!
