# MindComponent localization

comp-mind-ghosting-prevented = Вы не можете стать призраком в данный момент.

## Messages displayed when a body is examined and in a certain state

comp-mind-examined-catatonic = { CAPITALIZE($ent) } в кататоническом ступоре. Зрачки расширены и мутны. Восстановление маловероятно.
comp-mind-examined-dead = Тело совсем не движется.
comp-mind-examined-ssd = { CAPITALIZE($ent) } рассеяно смотрит в пустоту и ни на что не реагирует. { CAPITALIZE($ent) } может скоро придти в себя.
comp-mind-examined-dead-and-ssd = Зрачки { $ent } выглядят мутными.
comp-mind-examined-dead-and-irrecoverable = Зрачки { CAPITALIZE($ent) } расширены и мутны. Восстановление маловероятно.
