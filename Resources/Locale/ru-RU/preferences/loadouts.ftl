# Name
loadout-name-edit-label = Настраиваемое имя
loadout-name-edit-tooltip = Не более 32 символов. Если имя не указано, оно будет выбрано случайным образом.
# Restrictions
loadout-restrictions = Ограничения
loadouts-min-limit = Минимум: { $count }
loadouts-max-limit = Максимум: { $count }
loadouts-points-limit = Очки: { $count } / { $max }
loadouts-points-restriction = Недостаточно очков
