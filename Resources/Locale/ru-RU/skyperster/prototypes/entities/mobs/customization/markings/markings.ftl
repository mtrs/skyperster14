#tajaran
marking-TajaTail = Хвост таяры
marking-TajaEar = Таяран, уши
marking-TajaTailStripes = Хвост таяры (полоски)
marking-TajaranSnout = Таяран, морда
marking-TajaranSnout2 = Таяран, морда (альт)
marking-TajaranNose = Таяран, нос
marking-TajaHeadPatch = Таяран, голова (пятна)
marking-TajaHeadPoints = Таяран, голова (градиент)
marking-TajaTigerHead = Таяран, голова (тигр)
marking-TajaTigerFace = Таяран, голова (альт. тигр)
marking-TajaInEars = Таяран, уши (внутр)
marking-TajaOutEars = Таяран, уши (наружн)
marking-TajaraMuzinears = Таяран, голова (морда и уши)
marking-PointsTaja = Тело, градиент
marking-PatchTaja = Тело, пятна
marking-TajaraBellyFull = Живот (полный)
marking-TajaraBelly = Живот
marking-TajaraCrest = Грудь
marking-TajaEar-ears_plain_s = Наружная часть
marking-TajaEar-inears_s = Внутренняя часть
marking-TajaranSnout-muzzle_s = Морда
marking-TajaraCrest-crest_s = Грудь
marking-TajaraBelly-belly_s = Живот
marking-TajaTail-tajtail = Хвост
marking-TajaTailStripes-default_wingler_s = Полосы
marking-PatchTaja-patch_s = Пятна
marking-PointsTaja-points_s = Градиент
marking-TajaranNose-nose_s = Нос
marking-TajaTigerHead-tiger_head_s = Полосы
marking-TajaTigerFace-tiger_face_s = Полосы
marking-TajaraBellyFull-fullbelly_s = Живот

#lizard
marking-LizardTailSog = Унатх, хвост (короткий)
marking-LizardTailSog-sogtail = Хвост

#slime
marking-SlimeLizardBackspikes = Унатх, шипы
marking-SlimeLizardUnderbelly = Унатх, подбрюшье
marking-SlimeLizardSnoutRound = Унатх, морда (круглая)
marking-SlimeLizardSnoutSharp = Унатх, морда (заострённая)
