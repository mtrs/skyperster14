using Content.Server.Sirnea.Components;
using Content.Shared.Chemistry.Reagent;
using Robust.Shared.Prototypes;
using Content.Server.EntityEffects.EffectConditions;
using Content.Server.EntityEffects.Effects;
using Content.Shared.EntityEffects;

namespace Content.Server.Sirena.Chemistry.ReagentEffects;

public sealed partial class AddPushMarkup : EntityEffect
{
    [DataField(required: true)]
    public string[] Text;

    public override void Effect(EntityEffectBaseArgs args)
    {
        var entityManager = args.EntityManager;

        var comp = entityManager.EnsureComponent<PushMarkupEffectComponent>(args.TargetEntity);
        comp.OnExamined = Text;
    }
    protected override string? ReagentEffectGuidebookText(IPrototypeManager prototype, IEntitySystemManager entSys)
        => Loc.GetString("reagent-effect-guidebook-create-add-pushmarkup-effect");
}
