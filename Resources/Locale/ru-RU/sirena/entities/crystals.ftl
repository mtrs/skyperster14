ent-BaseCrystal = базовый кристалл
    .desc = Сияющее плотное скопление минерала. Вы можете увидеть своё отражение в нем.

ent-BaseCrystalShard = базовый осколок кристалл
    .desc = Небольшой осколок сиящего кристалла лишь слабо осветит ваш путь.

ent-ClothingCheapPendantBase = кулон из базового кристалла
    .desc = Кулончик из сияющего кристалла и тонкого провода.

# Blue

ent-CrystalBlueMedium = { ent-CrystalBlue }
    .desc = { ent-BaseCrystal.desc }

ent-CrystalBlueSmall = { ent-CrystalBlue }
    .desc = { ent-BaseCrystal.desc }

ent-CrystalShardBlue = осколок синего кристалла
    .desc = { ent-BaseCrystalShard.desc }

ent-ClothingCheapPendantBlue = кулон из синего кристалла
    .desc = { ent-ClothingCheapPendantBase.desc }

# Cyan

ent-CrystalCyanMedium = { ent-CrystalCyan }
    .desc = { ent-BaseCrystal.desc }

ent-CrystalCyanSmall = { ent-CrystalCyan }
    .desc = { ent-BaseCrystal.desc }

ent-CrystalShardCyan = осколок голубого кристалла
    .desc = { ent-BaseCrystalShard.desc }

ent-ClothingCheapPendantCyan = кулон из голубого кристалла
    .desc = { ent-ClothingCheapPendantBase.desc }

# Green
ent-CrystalGreenMedium = { ent-CrystalGreen }
    .desc = { ent-BaseCrystal.desc }

ent-CrystalGreenSmall = { ent-CrystalGreen }
    .desc = { ent-BaseCrystal.desc }

ent-CrystalShardGreen = осколок зелёного кристалла
    .desc = { ent-BaseCrystalShard.desc }

ent-ClothingCheapPendantGreen = кулон из зелёного кристалла
    .desc = { ent-ClothingCheapPendantBase.desc }

# Grey

ent-CrystalGreyMedium = { ent-CrystalGrey }
    .desc = { ent-BaseCrystal.desc }

ent-CrystalGreySmall = { ent-CrystalGrey }
    .desc = { ent-BaseCrystal.desc }

ent-CrystalShardGrey = осколок серого кристалла
    .desc = { ent-BaseCrystalShard.desc }

ent-ClothingCheapPendantGrey = кулон из серого кристалла
    .desc = { ent-ClothingCheapPendantBase.desc }

# Orange

ent-CrystalOrangeMedium = { ent-CrystalOrange }
    .desc = { ent-BaseCrystal.desc }

ent-CrystalOrangeSmall = { ent-CrystalOrange }
    .desc = { ent-BaseCrystal.desc }

ent-CrystalShardOrange = осколок оранжевого кристалла
    .desc = { ent-BaseCrystalShard.desc }

ent-ClothingCheapPendantOrange = кулон из оранжевого кристалла
    .desc = { ent-ClothingCheapPendantBase.desc }

# Pink

ent-CrystalPinkMedium = { ent-CrystalPink }
    .desc = { ent-BaseCrystal.desc }

ent-CrystalPinkSmall = { ent-CrystalPink }
    .desc = { ent-BaseCrystal.desc }

ent-CrystalShardPink = осколок розового кристалла
    .desc = { ent-BaseCrystalShard.desc }

ent-ClothingCheapPendantPink = кулон из розового кристалла
    .desc = { ent-ClothingCheapPendantBase.desc }
