using Content.Shared.Chemistry.Reagent;
using Content.Shared.Humanoid;
using Robust.Shared.Prototypes;
using Content.Server.EntityEffects.EffectConditions;
using Content.Server.EntityEffects.Effects;
using Content.Shared.EntityEffects;

namespace Content.Server.Sirena.Chemistry.ReagentEffectsCondition;

public sealed partial class SexCondition : EntityEffectCondition
{
    [DataField]
    public Sex Sex = default!;

    [DataField]
    public bool ShouldHave = true;

    public override bool Condition(EntityEffectBaseArgs args)
    {
        return args.EntityManager.GetComponent<HumanoidAppearanceComponent>(args.TargetEntity).Sex == Sex
            ? ShouldHave
            : !ShouldHave;
    }

    public override string GuidebookExplanation(IPrototypeManager prototype)
    {
        return Loc.GetString("reagent-effect-condition-guidebook-sex", ("sex", Sex), ("shouldHave", ShouldHave));
    }
}
